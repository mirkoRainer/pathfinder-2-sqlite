ability_mods:
  cha_mod: 3
  con_mod: 3
  dex_mod: 2
  int_mod: 1
  str_mod: 5
  wis_mod: 3
ac: 28
ac_special: null
alignment: LE
automatic_abilities:
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 26
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 26 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon raises its wing, gaining a +2 circumstance bonus to AC against
    the triggering attack. If the dragon is Flying, it descends 10 feet after the
    attack is complete.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Wing Deflection
  range: null
  raw_description: '**Wing Deflection** [Reaction] **Trigger** The dragon is targeted
    with an attack. **Effect** The dragon raises its wing, gaining a +2 circumstance
    bonus to AC against the triggering attack. If the dragon is Flying, it descends
    10 feet after the attack is complete.'
  requirements: null
  success: null
  traits: null
  trigger: The dragon is targeted with an attack.
description: 'Blue dragons are the sleek and poised cosmopolitans of the chromatic
  dragons. Their brand of evil is organized, manipulative, and regal. Blue dragons
  often lair near or within cities and set out to bend the population to their will
  and gather hordes of servants. These dragons love spinning webs of conspiracy. A
  blue dragon''s lackeys typically don''t even realize that they serve a dragon, but
  instead think the protection money, tariffs, or taxes they are amassing is treasure
  for a cruel but legitimate master. In some ways, blue dragons even see their servants
  as a living hoard and value them like treasure. These dragons have been known to
  use these tactics even with their own chromatic cousins.




  Not all blue dragons work clandestinely. Some lord over desert tribes and hill people
  like vengeful gods, demanding both tribute and worship. No matter how blue dragons
  manage their underlings, their bearing is regal and their lairs palatial; they''re
  universally intolerant of insubordination, incompetence, and embezzlement, and punish
  perpetrators with murderous efficiency.




  Blue dragons are also known for their use and mastery of illusion magic. They make
  use of illusions to augment their manipulations and bewilder their foes in battle.
  Blue dragons also have some control over water, but use this ability to destroy
  water, something quite dangerous for those who encounter them in their desert lairs.




  The ideal lair for a blue dragon contains multiple passages, rooms, and secret chambers.
  As social creatures, blue dragons prefer to host guests in comfort—but their dwellings
  should not be so public that just anyone can come calling. A force of guards keeps
  out intruders, and clever illusions conceal the edifice from prying eyes. Rather
  than a pile of coins or gems, a blue dragon''s true hoard is the rich furnishings
  in its citadel— expensive art, ornate furniture, and architectural marvels.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 26'
hp: 170
hp_misc: null
immunities:
- electricity
- paralyzed
- sleep
items: null
languages:
- Common
- Draconic
level: 9
melee:
- action_cost: One Action
  damage:
    formula: 2d8+11
    type: piercing
  name: jaws
  plus_damage:
  - formula: 1d12
    type: electricity
  to_hit: 21
  traits:
  - electricity
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 2d8+11
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 21
  traits:
  - agile
- action_cost: One Action
  damage:
    formula: 2d8+9
    type: bludgeoning
  name: tail
  plus_damage: null
  to_hit: 19
  traits:
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 1d8+9
    type: piercing
  name: horns
  plus_damage: null
  to_hit: 19
  traits:
  - reach 10 feet
name: Young Blue Dragon
perception: 18
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon breathes lightning that deals 5d12 electricity damage in
    an 80-foot line (DC 28 basic Reflex save). It can't use Breath Weapon again for
    1d4 rounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: '**Breath Weapon** [Two Actions]  (__arcane__, __electricity__,
    __evocation__) The dragon breathes lightning that deals 5d12 electricity damage
    in an 80-foot line (DC 28 basic Reflex save). It can''t use Breath Weapon again
    for 1d4 rounds.'
  requirements: null
  success: null
  traits:
  - arcane
  - electricity
  - evocation
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When casting __create water__, the dragon can attempt to destroy liquid
    instead of creating it, turning an equal amount of liquid into sand. This destroys
    liquid magic or alchemical items if they're of a lower level than the dragon (a
    creature can attempt a DC 26 Will save to protect all liquids in its possession).
    This doesn't affect the liquids in a creature's body.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Desert Thirst
  range: null
  raw_description: '**Desert Thirst** (__arcane__, __transmutation__) When casting
    __create water__, the dragon can attempt to destroy liquid instead of creating
    it, turning an equal amount of liquid into sand. This destroys liquid magic or
    alchemical items if they''re of a lower level than the dragon (a creature can
    attempt a DC 26 Will save to protect all liquids in its possession). This doesn''t
    affect the liquids in a creature''s body.'
  requirements: null
  success: null
  traits:
  - arcane
  - transmutation
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one horns Strike in any order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one horns Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon recharges its Breath Weapon whenever it scores a critical
    hit with a Strike.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** The dragon recharges its Breath Weapon whenever
    it scores a critical hit with a Strike.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 19
  fort_misc: null
  misc: null
  ref: 18
  ref_misc: null
  will: 19
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon can mimic any sound it has heard. It must succeed at a Deception
    check with a +4 circumstance bonus to do so.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Sound Imitation
  range: null
  raw_description: '**Sound Imitation** The dragon can mimic any sound it has heard.
    It must succeed at a Deception check with a +4 circumstance bonus to do so.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +18
- darkvision
- scent (imprecise) 60 feet
size: Large
skills:
- bonus: 15
  misc: null
  name: 'Acrobatics '
- bonus: 16
  misc: null
  name: 'Arcana '
- bonus: 18
  misc: null
  name: 'Deception '
- bonus: 18
  misc: null
  name: 'Diplomacy '
- bonus: 18
  misc: null
  name: 'Intimidation '
- bonus: 16
  misc: null
  name: 'Society '
- bonus: 15
  misc: null
  name: 'Stealth '
- bonus: 16
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary
  page_start: 107
  page_stop: null
speed:
- amount: 30
  type: Land
- amount: 15
  type: burrow
- amount: 100
  type: fly
spell_lists:
- dc: 28
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 1
    spells:
    - frequency: at will, see desert thirst
      name: create water
      requirement: null
  - heightened_level: 4
    level: 0
    spells:
    - frequency: null
      name: ghost sound
      requirement: null
  to_hit: null
traits:
- LE
- Large
- Dragon
- Electricity
type: Creature
weaknesses: null
