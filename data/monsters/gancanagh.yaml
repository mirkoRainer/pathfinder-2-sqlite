ability_mods:
  cha_mod: 5
  con_mod: 3
  dex_mod: 5
  int_mod: 2
  str_mod: 1
  wis_mod: 1
ac: 21
ac_special: null
alignment: CG
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A gancanagh's lungs can't tolerate smoke. They take a -2 circumstance
    penalty to saving throws against effects that create some form of smoke.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Vulnerable to Smoke
  range: null
  raw_description: '**Vulnerable to Smoke** A gancanagh''s lungs can''t tolerate smoke.
    They take a -2 circumstance penalty to saving throws against effects that create
    some form of smoke.'
  requirements: null
  success: null
  traits: null
  trigger: null
description: 'Gancanaghs are lovers, revelers, and dashing duelists of Elysium. Embodiments
  of free love, they eagerly throw themselves into courting targets for brief but
  earnest flings until their quicksilver passions change their desires. They serve
  Cayden Cailean as well as other bacchanalian deities and empyreal lords of Elysium
  who understand their desires for love and parties. Gancanaghs hate evil beings that
  profane the spirit of romance and passion, as such creatures (especially the demonic
  tempters known as succubi) reinforce stigmas against open and free love. One can
  give no greater insult to a gancanagh than to mistake him for such a creature, and
  more than one hotheaded gancanagh has challenged a misinformed paladin or other
  champion of good to a duel over such a slight. While they enjoy drinking and carousing,
  gancanaghs can''t stand smoke. Nonetheless, many gancanaghs carry whimsical-looking
  smoking pipes because they think it makes them look dapper. They cherish their silver
  flutes, for they enjoy the beauty of flutes'' music and its ability to sway the
  heart.




  The majority of gancanaghs present themselves as male, but the concept of gender
  to a creature like a gancanagh, which can change its shape freely, is much more
  fluid and open to interpretation than for many mortals. Gancanaghs enjoy using this
  flexibility to confront and test mortals'' convictions when faced with fear or prejudice,
  but when encountering mortals who themselves are open-minded about sexuality or
  gender identity, they can become lifelong allies. For those who are persecuted for
  such reasons, gancanaghs are tireless defenders and eager supporters, quick to provide
  safety and to punish those who would attempt to impose narrower beliefs upon a world
  that deserves more diversity than it often gets. If possible, a gancanagh seeks
  to educate and redeem those who hold destructive beliefs or prejudices, resorting
  to combat only to defend themself or an endangered mortal, or when no other option
  seems tenable—yet even then, they fight with sadness.




  **__Recall Knowledge - Celestial__ (__Religion__)**: DC 19'
hp: 75
hp_misc: null
immunities: null
items:
- silver rapier
- silver virtuoso flute
languages:
- Celestial
- Draconic
- Infernal
- tongues
level: 4
melee:
- action_cost: One Action
  damage:
    formula: 1d6+7
    type: piercing
  name: silver rapier
  plus_damage:
  - formula: 1d4
    type: good
  to_hit: 13
  traits:
  - deadly 1d10
  - disarm
  - finesse
  - good
  - magical
name: Gancanagh
perception: 11
proactive_abilities:
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The gancanagh can take on the appearance of any Small or Medium humanoid.
    This doesn't change their Speed or their attack and damage bonuses with their
    Strikes, but might change the damage type their Strikes deal (typically to bludgeoning).
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Change Shape
  range: null
  raw_description: '**Change Shape**   (__concentrate__, __divine__, __polymorph__,
    __transmutation__) The gancanagh can take on the appearance of any Small or Medium
    humanoid. This doesn''t change their Speed or their attack and damage bonuses
    with their Strikes, but might change the damage type their Strikes deal (typically
    to bludgeoning).'
  requirements: null
  success: null
  traits:
  - concentrate
  - divine
  - polymorph
  - transmutation
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The gancanagh embraces or kisses a willing creature, infusing that
    creature with their invigorating passion. The creature gains a +1 status bonus
    to attack rolls and 10 temporary Hit Points for 10 minutes. After that time, the
    target becomes fatigued for 10 minutes unless they succeed at a DC 21 Fortitude
    save.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Invigorating Passion
  range: null
  raw_description: '**Invigorating Passion** [Two Actions]  (__divine__, __emotion__,
    __enchantment__, __mental__) The gancanagh embraces or kisses a willing creature,
    infusing that creature with their invigorating passion. The creature gains a +1
    status bonus to attack rolls and 10 temporary Hit Points for 10 minutes. After
    that time, the target becomes fatigued for 10 minutes unless they succeed at a
    DC 21 Fortitude save.'
  requirements: null
  success: null
  traits:
  - divine
  - emotion
  - enchantment
  - mental
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 9
  fort_misc: null
  misc: null
  ref: 13
  ref_misc: null
  will: 11
  will_misc: null
sense_abilities: null
senses:
- Perception +11
- darkvision
size: Medium
skills:
- bonus: 9
  misc: null
  name: 'Athletics '
- bonus: 13
  misc: null
  name: 'Deception '
- bonus: 13
  misc: null
  name: 'Diplomacy '
- bonus: 14
  misc: null
  name: 'Performance '
- bonus: 9
  misc: null
  name: 'Religion '
- bonus: 11
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 31
  page_stop: null
speed:
- amount: 30
  type: Land
spell_lists:
- dc: 23
  misc: null
  name: Divine Innate Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: null
      name: suggestion
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: heroism
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: null
      name: heal
      requirement: null
    - frequency: at will
      name: mirror image
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: at will
      name: charm
      requirement: null
  - heightened_level: 5
    level: -1
    spells:
    - frequency: null
      name: tongues
      requirement: null
  to_hit: null
traits:
- CG
- Medium
- Azata
- Celestial
type: Creature
weaknesses:
- amount: 5
  type: cold iron
- amount: 5
  type: evil
