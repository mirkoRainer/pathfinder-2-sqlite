ability_mods:
  cha_mod: 6
  con_mod: 6
  dex_mod: 5
  int_mod: 5
  str_mod: 9
  wis_mod: 5
ac: 40
ac_special: null
alignment: LN
automatic_abilities:
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 36
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 36 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon spits a glob of caustic salt water at the creature. The creature
    takes 7d6 acid damage (DC 36 basic Reflex save). On a failure, the concentrate
    action is disrupted.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Brine Spit
  range: null
  raw_description: '**Brine Spit [Reaction]** **Trigger **A creature the brine dragon
    observes within 30 feet uses a __concentrate__ action; **Effect **The dragon spits
    a glob of caustic salt water at the creature. The creature takes 7d6 acid damage
    (DC 36 basic Reflex save). On a failure, the concentrate action is disrupted.'
  requirements: null
  success: null
  traits: null
  trigger: A creature the brine dragon observes within 30 feet uses a __concentrate__
    action
description: 'Brine dragons are usually blue-green in color, with shiny scales, crests
  that help them glide through the water, and sweeping neck frills. They care little
  for either good or evil. As they are both opinionated and willing to impose their
  sense of order on others, many brine dragons eventually seek to rule over a meticulously
  crafted community. These communities are orderly and well-planned, with rigid standards
  of courtesy and unchanging laws set down by the dragon themself. A settlement seeded
  by a brine dragon can be made of members of almost any ancestry, but the most common
  inhabitants are humans, merfolk, tengus, or sahuagin.




  Depending on the dragon''s personality, their community members might view their
  brine dragon ruler as anything from a benevolent force of order to a fearful tyrant.
  Regardless, the typical brine dragon has little patience for kindness or philanthropy,
  and the strength and health of their settlement as a whole are of greater concern
  than individuals'' well-being. A notable exception to this dispassion rises when
  an outside force encroaches on their lands. In these cases, the brine dragon is
  quick to step in and aid in the defense of their community.




  Although brine dragons enjoy cultivating settlements, they rarely make their lairs
  within the city limits, instead preferring to dwell in sea caves or cliffside grottoes
  overlooking the coastline. Here they can retreat for privacy as needed, or can accumulate
  and display their gathered wealth in a place where they feel safe spreading out
  their treasures. Brine dragon hoards often consist of a mix of offerings and taxes
  paid by those they rule over and strange discoveries salvaged from sunken ships.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 41


  **__Recall Knowledge - Elemental__ (__Arcana__, __Nature__)**: DC 41'
hp: 330
hp_misc: null
immunities:
- acid
- paralyzed
- sleep
items: null
languages:
- Aquan
- Common
- Draconic
- Sylvan
- Utopian
level: 17
melee:
- action_cost: One Action
  damage:
    formula: 3d10+17
    type: piercing
  name: jaws
  plus_damage:
  - formula: 5d4
    type: acid
  to_hit: 34
  traits:
  - acid
  - magical
  - reach 20 feet
- action_cost: One Action
  damage:
    formula: 3d10+17
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 34
  traits:
  - agile
  - magical
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 3d12+17
    type: bludgeoning
  name: tail
  plus_damage: null
  to_hit: 32
  traits:
  - magical
  - reach 25 feet
- action_cost: One Action
  damage:
    formula: 2d12+17
    type: piercing
  name: wing
  plus_damage: null
  to_hit: 32
  traits:
  - magical
  - reach 20 feet
name: Ancient Brine Dragon
perception: 32
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon breathes a spray of acidic salt water that deals 18d6 acid
    damage in a 120-foot line (DC 38 basic Reflex save). They can't use Breath Weapon
    again for 1d4 rounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: '**Breath Weapon** [Two Actions]  (__acid__, __evocation__, __primal__)
    The dragon breathes a spray of acidic salt water that deals 18d6 acid damage in
    a 120-foot line (DC 38 basic Reflex save). They can''t use Breath Weapon again
    for 1d4 rounds.'
  requirements: null
  success: null
  traits:
  - acid
  - evocation
  - primal
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The dragon tries to capsize an adjacent aquatic vessel of their size
    or smaller. They must succeed at an __Athletics__ check with a DC of 35 (reduced
    by 5 for each size smaller the vessel is than the dragon) or the pilot's __Sailing
    Lore__ DC, whichever is higher.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Capsize
  range: null
  raw_description: '**Capsize**   (__attack__) The dragon tries to capsize an adjacent
    aquatic vessel of their size or smaller. They must succeed at an __Athletics__
    check with a DC of 35 (reduced by 5 for each size smaller the vessel is than the
    dragon) or the pilot''s __Sailing Lore__ DC, whichever is higher.'
  requirements: null
  success: null
  traits:
  - attack
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes a jaws Strike. On a hit, the target takes 6d6 __persistent
    acid damage__, and is __sickened 3__ from the pain of salt and brine in its wounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Desiccating Bite
  range: null
  raw_description: '**Desiccating Bite** [Two Actions]  The dragon makes a jaws Strike.
    On a hit, the target takes 6d6 __persistent acid damage__, and is __sickened 3__
    from the pain of salt and brine in its wounds.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one wing Strike in any order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one wing Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon recharges their Breath Weapon whenever they score a critical
    hit with a Strike.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** The dragon recharges their Breath Weapon
    whenever they score a critical hit with a Strike.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The brine dragon's body is encrusted with salty, acidic crystals. When
    a target takes damage from one of the dragon's melee Strikes, it must succeed
    at a DC 38 Fortitude Save or be __stunned 1__ (stunned 3 on a critical failure).
    The target then becomes temporarily immune for 1 minute.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Painful Strikes
  range: null
  raw_description: '**Painful Strikes** (__acid__) The brine dragon''s body is encrusted
    with salty, acidic crystals. When a target takes damage from one of the dragon''s
    melee Strikes, it must succeed at a DC 38 Fortitude Save or be __stunned 1__ (stunned
    3 on a critical failure). The target then becomes temporarily immune for 1 minute.'
  requirements: null
  success: null
  traits:
  - acid
  trigger: null
ranged: null
rarity: Rare
resistances: null
ritual_lists: null
saves:
  fort: 31
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 30
  ref_misc: null
  will: 30
  will_misc: null
sense_abilities: null
senses:
- Perception +32
- darkvision
- scent (imprecise) 60 feet
size: Gargantuan
skills:
- bonus: 28
  misc: null
  name: 'Acrobatics '
- bonus: 32
  misc: null
  name: 'Athletics '
- bonus: 32
  misc: null
  name: 'Deception '
- bonus: 34
  misc: null
  name: 'Intimidation '
- bonus: 30
  misc: null
  name: 'Nature '
- bonus: 30
  misc: null
  name: 'Society '
- bonus: 29
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary 2
  page_start: 88
  page_stop: null
speed:
- amount: 50
  type: Land
- amount: 140
  type: fly
- amount: 70
  type: swim
spell_lists:
- dc: 38
  misc: ''
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 6
    spells:
    - frequency: null
      name: hydraulic torrent
      requirement: null
  - heightened_level: null
    level: 5
    spells:
    - frequency: ×3
      name: control water
      requirement: null
    - frequency: null
      name: mariner's curse
      requirement: null
  - heightened_level: null
    level: 4
    spells:
    - frequency: at will
      name: hydraulic push
      requirement: null
    - frequency: at will
      name: obscuring mist
      requirement: null
  to_hit: 30
traits:
- Rare
- LN
- Gargantuan
- Amphibious
- Dragon
- Elemental
- Water
type: Creature
weaknesses: null
