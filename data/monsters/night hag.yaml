ability_mods:
  cha_mod: 3
  con_mod: 6
  dex_mod: 4
  int_mod: 4
  str_mod: 5
  wis_mod: 5
ac: 28
ac_special: null
alignment: NE
automatic_abilities: null
description: 'Night hags are thieves and merchants of mortal souls. These foul creatures
  collect souls in dark gems or crystalline jars to sell in fiendish markets, and
  are themselves empowered by potent magic jewels known as heartstones. They haunt
  the Ethereal Plane, where they prey upon mortals in their dreams, debilitating them
  with horrific nightmares as they rest. A night hag may find a particular target
  and haunt them continuously over the course of weeks, slowly and cruelly breaking
  down the victim''s will and ability to resist, until their soul is forfeit.




  A night hag is a canny mastermind and soul broker, willing to consider any deal
  as long as she is convinced she has the upper hand. Although a night hag finds it
  easy to travel the Ethereal Plane and prey upon helpless souls that can''t fight
  back, these souls are also the least desirable to the evil outsiders the night hag
  bargains with, and so a night hag gathers allies and minions that allow her to prey
  on more potent souls without personally risking herself. Their favored minions are
  nightmares, with whom they share a special bond. Above all, night hags avoid fighting
  foes that can harry them on the Ethereal Plane, picking fights only when they are
  certain they can escape.




  **__Recall Knowledge - Fiend__ (__Religion__)**: DC 26


  **__Recall Knowledge - Humanoid__ (__Society__)**: DC 26'
hp: 170
hp_misc: null
immunities:
- sleep
items:
- heartstone
languages:
- Abyssal
- Aklo
- Celestial
- Common
- Infernal
level: 9
melee:
- action_cost: One Action
  damage:
    formula: 2d8+8
    type: piercing
  name: jaws
  plus_damage:
  - formula: 1d6
    type: evil and Abyssal plague
  to_hit: 20
  traits:
  - magical
- action_cost: One Action
  damage:
    formula: 2d10+8
    type: slashing
  name: claw
  plus_damage:
  - formula: 1d6
    type: evil
  to_hit: 20
  traits:
  - agile
  - magical
name: Night Hag
perception: 18
proactive_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A creature can't recover from drained until abyssal plague is cured.
    **Saving Throw** DC 28 Fortitude; **Stage 1** __Drained 1__ (1 day); **Stage 2**
    __Drained__ increases by 2 (1 day)
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Abyssal Plague
  range: null
  raw_description: '**Abyssal Plague** (__disease__) A creature can''t recover from
    drained until abyssal plague is cured. **Saving Throw** DC 28 Fortitude; **Stage
    1** __Drained 1__ (1 day); **Stage 2** __Drained__ increases by 2 (1 day)'
  requirements: null
  success: null
  traits:
  - disease
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The night hag can take on the appearance of any Medium female humanoid.
    This doesn't change her Speed or her attack and damage bonuses with her Strikes,
    but might change the damage type her Strikes deal (typically to bludgeoning).
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Change Shape
  range: null
  raw_description: '**Change Shape**   (__concentrate__, __occult__, __polymorph__,
    __transmutation__) The night hag can take on the appearance of any Medium female
    humanoid. This doesn''t change her Speed or her attack and damage bonuses with
    her Strikes, but might change the damage type her Strikes deal (typically to bludgeoning).'
  requirements: null
  success: null
  traits:
  - concentrate
  - occult
  - polymorph
  - transmutation
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: If a night hag is ethereal and hovering over a sleeping chaotic or
    evil creature, she can ride the victim's back until dawn. The creature endures
    tormenting dreams as the hag casts __nightmare__ on it, and is exposed to abyssal
    plague. Any drained caused by dream haunting is cumulative. Only an ethereal being
    can confront the night hag and stop her dream haunting.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Dream Haunting
  range: null
  raw_description: '**Dream Haunting** (__enchantment__, __occult__, __mental__) If
    a night hag is ethereal and hovering over a sleeping chaotic or evil creature,
    she can ride the victim''s back until dawn. The creature endures tormenting dreams
    as the hag casts __nightmare__ on it, and is exposed to abyssal plague. Any drained
    caused by dream haunting is cumulative. Only an ethereal being can confront the
    night hag and stop her dream haunting.'
  requirements: null
  success: null
  traits:
  - enchantment
  - occult
  - mental
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A creature __flat-footed__ to the night hag takes a -2 circumstance
    penalty to checks and DCs to defend against her spells.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Spell Ambush
  range: null
  raw_description: '**Spell Ambush** A creature __flat-footed__ to the night hag takes
    a -2 circumstance penalty to checks and DCs to defend against her spells.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Common
resistances:
- amount: 10
  type: mental
ritual_lists: null
saves:
  fort: 19
  fort_misc: null
  misc: +2 status to all saves vs. magic, -2 to all saves if the night hag does not
    have her <i>heartstone</i>
  ref: 17
  ref_misc: null
  will: 18
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A night hag adds __dominate__, __nightmare__, __scrying__, and __spellwrack__
    to her coven's spells.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Coven
  range: null
  raw_description: '**Coven** A night hag adds __dominate__, __nightmare__, __scrying__,
    and __spellwrack__ to her coven''s spells.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'When a night hag rides a nightmare, the nightmare also gains the night
    hag''s status bonus to saves against magic, and both the hag and rider benefit
    when the night hag uses her heartstone''s __ethereal jaunt__ innate spell.


    '
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Nightmare Rider
  range: null
  raw_description: '**Nightmare Rider** When a night hag rides a nightmare, the nightmare
    also gains the night hag''s status bonus to saves against magic, and both the
    hag and rider benefit when the night hag uses her heartstone''s __ethereal jaunt__
    innate spell.


    '
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +18
- darkvision
size: Medium
skills:
- bonus: 18
  misc: null
  name: 'Arcana '
- bonus: 18
  misc: null
  name: 'Deception '
- bonus: 18
  misc: null
  name: 'Diplomacy '
- bonus: 14
  misc: null
  name: 'Intimidation '
- bonus: 20
  misc: null
  name: 'Occultism '
- bonus: 20
  misc: null
  name: 'Religion '
source:
- abbr: Bestiary
  page_start: 202
  page_stop: null
speed:
- amount: 25
  type: Land
spell_lists:
- dc: 28
  misc: null
  name: Occult Innate Spells
  spell_groups:
  - heightened_level: null
    level: 9
    spells:
    - frequency: at will, from heartstone
      name: bind soul
      requirement: null
    - frequency: at will, from heartstone
      name: ethereal jaunt
      requirement: null
  - heightened_level: null
    level: 8
    spells:
    - frequency: null
      name: dream council
      requirement: null
  - heightened_level: null
    level: 5
    spells:
    - frequency: null
      name: nightmare
      requirement: null
    - frequency: x2, from heartstone
      name: shadow blast
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: at will
      name: dream message
      requirement: null
    - frequency: at will
      name: magic missile
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: invisibility
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: at will
      name: ray of enfeeblement
      requirement: null
    - frequency: at will
      name: sleep
      requirement: null
  - heightened_level: 3
    level: -1
    spells:
    - frequency: null
      name: detect magic
      requirement: null
  - heightened_level: 2
    level: -1
    spells:
    - frequency: all alignments simultaneously
      name: detect alignment
      requirement: null
  to_hit: null
traits:
- NE
- Medium
- Fiend
- Hag
- Humanoid
type: Creature
weaknesses:
- amount: 10
  type: cold iron
